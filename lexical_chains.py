# -*- coding: utf-8 -*-
"""
===============================================================================
              Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""

# Enabling or Disabling the pistemming could be an option
# Lemmatize or not
# Lemmatize with a POS tag 


from LexicalGraph import LexicalGraph
from PreProcessor import Splitter, LemmatizationWithPOSTagger

splitter = Splitter()
lemmatization_using_pos_tagger = LemmatizationWithPOSTagger()

#text = "dog is a pet animal. pets love humans. dogs eat meat and hates cats. cats are also pet animals. cats hats mouse. mice are called rodents. hounds can bite when they are angry. tooth has to be taken care. human woman women person man men child baby adult"
#text = "dog man cat"
#text = "Studies have also shown that certain colors can have an impact on performance. No one likes to see a graded test covered in red ink, but one study found that seeing the color red before taking an exam actually hurt test performance. While the color red is often described as threatening, arousing or exciting, many previous studies on the impact of the color red have been largely inconclusive. The study found, however, that exposing students to the color red prior to an exam has been shown to have a negative impact on test performance"
#text = "Rice can be cultivated by different methods based on the type of region. But in India, the traditional methods are still in use for harvesting rice. The fields are initially ploughed and then fertiliser is applied which typically consists of cow dung and then the field is smoothed. The seeds are transplanted by hand and then through proper irrigation, the seeds are cultivated. Rice grows on a variety of soils like silts, loams and gravels."
text = "Dog ate the cat. Cat ate the mouse. The Man had the cat as a pet. The Cat always eats mice. Cat loves milk. Cat is a carnivore."

#step 1 split document into sentence followed by tokenization
tokens = splitter.split(text)
    
#step 2 lemmatization using pos tagger 
lemma_pos_token = lemmatization_using_pos_tagger.pos_tag(tokens)

filter_stop_words = True

LG = LexicalGraph([token_tuple[1] for token_tuple in lemma_pos_token], filter_stop_words )
LG.updateLinks()
LG.printChains()

#chains = [ Chain() ]
#
#for wi in range(1,len(final_sentence)):
#    w = final_sentence[wi]
#    # see if we can add this to the existing chains
#    addedInChain = 0
#    for c in chains:
#        if c.addWord(w):
#            addedInChain = 1
#            break
#    if (addedInChain is 0):
#        # create a new chain and add this word
#        newChain = Chain()
#        newChain.forceAddWord(w)
#        chains.append(newChain)
#
#for c in chains:
#    # length 1 objects are not chains, donot print them
#    if len(c.data) > 1:
#        c.printDetails()
# https://stackoverflow.com/questions/25534214/nltk-wordnet-lemmatizer-shouldnt-it-lemmatize-all-inflections-of-a-word/25544239
