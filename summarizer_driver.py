# -*- coding: utf-8 -*-
"""
===============================================================================
              Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""

from TextSummarizer import TextSummarizer, SummarizerSentenceControl

import sys
import urllib2
from bs4 import BeautifulSoup
import random
import os
from os import listdir
from os.path import isfile, join

# Code copied from the following link.
# https://glowingpython.blogspot.com/2014/09/text-summarization-with-nltk.html
def get_only_text(url):
     """ 
      return the title and the text of the article
      at the specified url
     """
     page = urllib2.urlopen(url).read().decode('utf8')
     soup = BeautifulSoup(page)
     text = ' '.join(map(lambda p: p.text, soup.find_all('p')))
     return soup.title.text, text

# Returns the contents of the file identified by the |file_name| parameter.
# |path| contains the path to the file.     
def get_file_contents(path, file_name):
    with open(join(path, file_name), 'r') as f:
        contents = f.read()
    return contents

# Creates the output directories as specified in the |results_info| dictionary.
# The |output_dir| is the path where the output directories are created.    
def setup_output_directories(output_dir, results_info):
    for key, output_file_path in results_info.items():
        dir_path = join(output_dir, output_file_path)
        try:
            os.stat(dir_path)
        except:
            os.mkdir(dir_path)       

# Helper function to generate the summary for the file name in the directory
# passed in. The summary is written to the output directory.
def generate_summary(dir_path, file_name, results_info):
    summarizer = TextSummarizer()
    contents = get_file_contents(dir_path, file_name)
    for control, output_file_path in results_info.items():
        print '\nINFO: Generating summary for file name {} with control'.format(file_name, control)
        print 'INFO: summary will be available at {}'.format(output_file_path)
        output_path = output_file_path
        summary = summarizer.generateSummary(contents, control)
        summarizer.printSummary()
        output_file_name = join(output_path, "summary_" + os.path.basename(file_name))
        with open(output_file_name, "w") as text_file:
            text_file.write(summary)

path = "."

# We generate two separate summaries. First one is the default summary which is
# printed by walking the largest lexical chain, followed by the next one and so on
# The second summary is created by attempting to score sentences on similarity and
# dropping sentences which are similar to each other.
results_info = {}
results_info[SummarizerSentenceControl.DEFAULT] = "summary_default"
results_info[SummarizerSentenceControl.DROP_SIMILAR_SENTENCES] = "summary_remove_dups"
results_info[SummarizerSentenceControl.HOMOGENIZE_CHAIN_SCORES] = "summary_homogenity_index"
results_info[SummarizerSentenceControl.INCLUDE_LEVEL_ONE_RELATIONS] = "summary_include_level_one"

setup_output_directories(path, results_info)

if (len(sys.argv) == 2):
    if isfile(join(path, sys.argv[1])):
        generate_summary(path, sys.argv[1], results_info)
    elif os.path.isdir(join(path, sys.argv[1])):
        dir_path = join(path, sys.argv[1])
        files = [f for f in listdir(dir_path) if isfile(join(dir_path, f))]
        print(files)
        for file_name in files:
            generate_summary(dir_path, file_name, results_info)
    else:
        print("Invalid file or directory name: %s" %(sys.argv[1]))
        sys.exit()
else:
    # Code copied from the following link.
    # https://glowingpython.blogspot.com/2014/09/text-summarization-with-nltk.html
    feed_xml = urllib2.urlopen('http://feeds.bbci.co.uk/news/rss.xml').read()
    feed = BeautifulSoup(feed_xml.decode('utf8'))
    to_summarize = map(lambda p: p.text, feed.find_all('guid'))
    random.seed(1000)
    selected = [ random.choice(to_summarize) ]
    #fs = FrequencySummarizer()
    for article_url in selected:
        summarizer = TextSummarizer()
        title, text = get_only_text(article_url)
        print u'INFO: Fetching article - {}'.format(title)
        summarizer.generateSummary(text, SummarizerSentenceControl.DEFAULT)
        print("\nSummary of text is as below:\n")
        summarizer.printSummary()
    
