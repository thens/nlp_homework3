The leaders of North and South Korea have agreed to work to rid the peninsula of nuclear weapons, after holding a historic summit.
The announcement was made by the North's Kim Jong-un and Moon Jae-in of South Korea after talks at the border.
The two also agreed to push towards turning the armistice that ended the Korean War in 1953 into a peace treaty this year.
The summit came just months after warlike rhetoric from North Korea.
Speaking at a banquet after Friday's talks, Mr Kim hailed the progress he said had been made.
"We bade farewell to the frozen relationship between North and South Korea, which was a nightmare. And we announced the beginning of a warm spring to the world," he said.
Historic summit as it happened
Five key moments from the meeting
Read the Korean declaration in full
What is in the agreement?
Details of how denuclearisation would be achieved were not made clear and many analysts remain sceptical about the North's apparent enthusiasm for engagement.
An issue for the North is the security guarantee extended by the US, a nuclear power, to South Korea and Japan and its military presence in both countries.
Previous inter-Korean agreements have included similar pledges but were later abandoned after the North resorted to nuclear and missile tests and the South elected more conservative presidents.
Mr Kim said the two leaders had agreed to work to prevent a repeat of the region's "unfortunate history" in which progress had "fizzled out".
"There may be backlash, hardship and frustration," he said, adding: "A victory cannot be achieved without pain."