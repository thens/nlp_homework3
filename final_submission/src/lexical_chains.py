# -*- coding: utf-8 -*-
"""
===============================================================================
              Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""

# Enabling or Disabling the pistemming could be an option
# Lemmatize or not
# Lemmatize with a POS tag 


from LexicalGraph import LexicalGraph
from PreProcessor import Splitter, LemmatizationWithPOSTagger
from os.path import isfile, join
import sys

# Returns the contents of the file identified by the |file_name| parameter.
# |path| contains the path to the file.     
def get_file_contents(path, file_name):
    with open(join(path, file_name), 'r') as f:
        contents = f.read()
    return contents

splitter = Splitter()
lemmatization_using_pos_tagger = LemmatizationWithPOSTagger()

path = "."

if (len(sys.argv) == 2):
    if isfile(join(path, sys.argv[1])):
        text = get_file_contents(path, sys.argv[1])
    else:
        print("Invalid file or directory name: %s" %(sys.argv[1]))
        sys.exit()
else:
    #text = "dog is a pet animal. pets love humans. dogs eat meat and hates cats. cats are also pet animals. cats hats mouse. mice are called rodents. hounds can bite when they are angry. tooth has to be taken care. human woman women person man men child baby adult"
    #text = "dog man cat"
    #text = "Studies have also shown that certain colors can have an impact on performance. No one likes to see a graded test covered in red ink, but one study found that seeing the color red before taking an exam actually hurt test performance. While the color red is often described as threatening, arousing or exciting, many previous studies on the impact of the color red have been largely inconclusive. The study found, however, that exposing students to the color red prior to an exam has been shown to have a negative impact on test performance"
    #text = "Rice can be cultivated by different methods based on the type of region. But in India, the traditional methods are still in use for harvesting rice. The fields are initially ploughed and then fertiliser is applied which typically consists of cow dung and then the field is smoothed. The seeds are transplanted by hand and then through proper irrigation, the seeds are cultivated. Rice grows on a variety of soils like silts, loams and gravels."
    text = "Dog ate the cat. Cat ate the mouse. The Man had the cat as a pet. The Cat always eats mice. Cat loves milk. Cat is a carnivore."
    
#step 1 split document into sentence followed by tokenization
tokens = splitter.split(text)
    
#step 2 lemmatization using pos tagger 
lemma_pos_token = lemmatization_using_pos_tagger.pos_tag(tokens)


print "\n\nINFO: Lexical chains without level one relations (grandparent-hypernyms/sibling-hyponyms)"
LG1 = LexicalGraph([token_tuple[1] for token_tuple in lemma_pos_token], include_level_one_relation=False )
LG1.updateLinks()
LG1.printChains()

print "\n\nINFO: Lexical chains including level one relations (grandparent-hypernyms/sibling-hyponyms)"
LG2 = LexicalGraph([token_tuple[1] for token_tuple in lemma_pos_token], include_level_one_relation=True )
LG2.updateLinks()
LG2.printChains()
