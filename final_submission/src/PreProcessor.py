# -*- coding: utf-8 -*-
"""
===============================================================================
              Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""

import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet as wn
from nltk.tokenize import sent_tokenize

#example text 
text = 'What can I say about this place. The staff of these restaurants is nice and the eggplant is not bad'

# This class maintains details about a word like which sentence it occurred in, etc.
# An instance of this class is created for every word we see in the tokenization phase.
class WordDetails(object):
    def __init__(self, word, sentence_number):
        self.word = word
        self.sentence_number = sentence_number
        self.count = 0
        
    def dump(self):
        print(self.word)
        print(self.sentence_number)
        print(self.count)
        
    def sentence_number(self):
        return self.sentence_number
    
    def __repr__(self):
        return "<word: %s, sentence number: %d>" % (self.word, self.sentence_number)

# https://stackoverflow.com/a/46564234/911759

class Splitter(object):
    """
    split the document into sentences and tokenize each sentence
    """
    def __init__(self):
        self.splitter = nltk.data.load('tokenizers/punkt/english.pickle')
        self.tokenizer = nltk.tokenize.TreebankWordTokenizer()
        self.word_dict = {}
        self.lemmatization_using_pos_tagger = LemmatizationWithPOSTagger()

    def split(self, text):
        """
        out : ['What', 'can', 'I', 'say', 'about', 'this', 'place', '.']
        """
        if type(text) != type(u'unicode'):
            text = unicode(text, "utf-8", errors='ignore')
        # split into single sentence
        sentences = self.splitter.tokenize(text.lower())
        
        return self.splitHelper(sentences)
    
    def splitBySentences(self, text):
        # convert to unicode
        if type(text) != type(u'unicode'):
            text = unicode(text, "utf-8", errors='ignore')
        # Split text by sentences and then regular tokenization.        
        sentences = sent_tokenize(text.lower())
        sentences_original_case = sent_tokenize(text)
        tokens = self.splitHelper(sentences)
        return tokens, self.word_dict, sentences_original_case
        
    def splitHelper(self, sentences):
        # tokenization in each sentence
        tokens = []
        self.word_dict = {}

        sentence_number = 0
        for sent in sentences:
            sentence_number = sentence_number + 1
            for t in self.tokenizer.tokenize(sent):
                tokens.append(t)
                # Add information about this word in the word dictionary like
                # which sentence it occurs in, etc.
                # The key in the dictionary is the lemma of the word or the original word
                # if it cannot be lemmatized.
                key = t
                tmp_list = []
                tmp_list.append(t)
                pos_tuple_list = self.lemmatization_using_pos_tagger.pos_tag(tmp_list)
                if (len(pos_tuple_list) > 0):
                    pos_tuple = pos_tuple_list[0]
                    key = pos_tuple[1]
                word_list = self.word_dict.get(key)
                if (word_list is None):
                    word_list = []
                word_list.append(WordDetails(t, sentence_number))
                self.word_dict[key] = word_list
        return tokens
        
    def getWordDictionary(self):
        return self.word_dict

class LemmatizationWithPOSTagger(object):
    def __init__(self):
        self.lemmatizer = WordNetLemmatizer()
    def get_wordnet_pos(self,treebank_tag):
        """
        return WORDNET POS compliance to WORDENT lemmatization (a,n,r,v) 
        """
        if treebank_tag.startswith('J'):
            return wn.ADJ
        elif treebank_tag.startswith('V'):
            return wn.VERB
        elif treebank_tag.startswith('N'):
            return wn.NOUN
        elif treebank_tag.startswith('R'):
            return wn.ADV
        else:
            # As default pos in lemmatization is Noun
            return wn.NOUN

    def pos_tag(self,tokens):
        # find the pos tagging for each tokens [('What', 'WP'), ('can', 'MD'), ('I', 'PRP') ....
        pos_tokens = nltk.pos_tag(tokens)

        # lemmatization using pos tagg   
        # convert into feature set of [('What', 'What', ['WP']), ('can', 'can', ['MD']), ... ie [original WORD, Lemmatized word, POS tag]
        # p1 = [[ (word, self.lemmatizer.lemmatize(word,self.get_wordnet_pos(pos_tag)), pos_tag) for (word,pos_tag) in pos] for pos in pos_tokens]
        p1 = []
        for word,pos_tag in pos_tokens:
            wordnet_pos = self.get_wordnet_pos(pos_tag)
            if wordnet_pos in (wn.NOUN, wn.ADJ):
                p1.append((word, self.lemmatizer.lemmatize(word,self.get_wordnet_pos(pos_tag)), pos_tag))
        return p1

