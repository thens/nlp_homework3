# -*- coding: utf-8 -*-
"""
===============================================================================
                   Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""

from nltk.stem import WordNetLemmatizer
from LexicalGraph import LexicalGraph
from PreProcessor import Splitter, LemmatizationWithPOSTagger, WordDetails
import time
from nltk.corpus import brown, stopwords
from nltk.cluster.util import cosine_distance
from enum import Enum

# printing with highlight
prettyPrintEnabled = False
try:
    from colorama import Fore,Style
    print "INFO: coloroma is installed, " + Fore.MAGENTA + "pretty printing " + Style.RESET_ALL + "is enabled"
    prettyPrintEnabled = True
except ImportError:
    print "INFO: coloroma is not installed, pretty printing is disabled"
    

class SummarizerSentenceControl(Enum):
    DEFAULT = 0
    DROP_SIMILAR_SENTENCES = 2
    INCLUDE_LEVEL_ONE_RELATIONS = 3
    HOMOGENIZE_CHAIN_SCORES = 4
    UNSUPPORTED = 5

# This class provides functionality to generate the summary of a text body
# using lexical chains.It uses the lexical graph to build the chains using word
# net and then generates the summary from the chains from the lexical graph.
# The chains are sorted by the number of words they contain.
class TextSummarizer(object):
    def __init__(self):
        self.paragraph = []
        self.graph = []
        self.proposed_summary = []
        self.proposed_index = []
        self.best_index = []
        self.best_summary = []
        self.splitter = Splitter()
        self.lemmatization_using_pos_tagger = LemmatizationWithPOSTagger()
        self.word_dict = []
        self.sentences = []
        self.total_lines_to_generate = 0
        self.drop_similar_sentences = False
        self.summarizer_sentence_control = SummarizerSentenceControl.DEFAULT
        self.summary_text = []
        
    # Generates summary fro the |text| passed in. The |total_lines| parameter
    # controls how many lines of summary we want to generate.
    # The generated summary is stored in the summary member field.
    def generateSummary(self, text, sentence_control, total_lines = 0):
        self.paragraph = text       
        self.total_lines_to_generate = total_lines
        self.summarizer_sentence_control = sentence_control
        self.summary_text = []
        
        if (sentence_control != SummarizerSentenceControl.DEFAULT and
            sentence_control != SummarizerSentenceControl.DROP_SIMILAR_SENTENCES and
            sentence_control != SummarizerSentenceControl.INCLUDE_LEVEL_ONE_RELATIONS and 
            sentence_control != SummarizerSentenceControl.HOMOGENIZE_CHAIN_SCORES):
            print("ERROR: Unsupported sentence control parameter passed %d" %(int(sentence_control)))
            print("ERROR: Falling back to DEFAULT")
            self.summarizer_sentence_control = SummarizerSentenceControl.DEFAULT
 
        # blank out summaries
        self.proposed_index = []
        self.proposed_summary = []
        self.best_summary = []
        self.best_index = []

        # First we tokenize the text by splitting it on sentences.       
        (self.tokens, self.word_dict, self.sentences) = self.splitter.splitBySentences(
                self.paragraph)
        
        #step 2 lemmatization using pos tagger 
        self.lemma_pos_token = self.lemmatization_using_pos_tagger.pos_tag(
                self.tokens)
        
        if (total_lines <= 0):
            self.total_lines_to_generate = len(self.sentences) // 4
            
        print 'INFO: Total summary lines to generate: {}'.format(self.total_lines_to_generate)
        
        # Build a graph by using the lexical chains for the input text using
        # Wordnet.
        if SummarizerSentenceControl.INCLUDE_LEVEL_ONE_RELATIONS:
            include_level_one_relation = True
        else:
            include_level_one_relation = False
        self.graph = LexicalGraph([token_tuple[1] for token_tuple in self.lemma_pos_token], include_level_one_relation = include_level_one_relation)

        start = time.time()
        print "INFO: Finding word relations"
        self.graph.updateLinks()
        end = time.time()
        print "INFO: completed in {:5.2f} seconds".format(end-start)
        print "INFO: Summarizing lexical chains"
        self.graph.printChains()

        # Get the word chains from the graph
        chains = self.graph.getChains()
        if SummarizerSentenceControl.HOMOGENIZE_CHAIN_SCORES:
            chain_ids = self.graph.getTopKChains(homogenity=True)
        else:
            chain_ids = self.graph.getTopKChains()
        
        self.summary = ""
        
        # Sort the chains based on decreasing order of word frequency, i.e the
        # longest chains appear first. We generate the summary from the longest chain
        # first followed by the next chain and so on.
        number_of_lines = 0
        
        for sorted_key in chain_ids:
            # If we are done with the required lines of summary break here.
            if (number_of_lines >= self.total_lines_to_generate):
                break

            # If we exhausted the number of sentencews bail
            if (number_of_lines >= len(self.sentences)):
                break
            
            print("\nINFO: Chain number %d. Length %d\n" % (sorted_key, len(chains[sorted_key]['words'])))
            
            # Sort the word list we got from the graph by increasing order of
            # sentences..
            #word_details_list = self.getWordListOrderedBySentence(chains[sorted_key])
            word_details_list = self.getWordListOrderedByFrequency(chains[sorted_key]['words'])
            
            for word_detail in word_details_list:
                # If we are done with the required lines break here.
                if (number_of_lines >= self.total_lines_to_generate):
                    break
                
                # If we exhausted the number of sentencews bail
                if (number_of_lines >= len(self.sentences)):
                    break
                
                # We have already seen this sentence. Bail here.
                if (word_detail.sentence_number - 1 in self.proposed_index):
                    continue

                self.proposed_summary.append(
                        self.sentences[word_detail.sentence_number - 1])
                self.proposed_index.append(word_detail.sentence_number -1)
                #self.summary = self.summary + self.sentences[word_detail.sentence_number - 1]
                number_of_lines = number_of_lines + 1

        self.proposed_index.sort()

        print("\nINFO: Proposed line numbers before scoring")
        print(self.proposed_index)
        self.scoreAndGetSummary()
        print("\nINFO: Actual line numbers retained after scoring")
        print(self.best_index)
        self.summary = " ".join(str(line) for line in self.best_summary)
        return self.summary
        
    def printSummary(self):
        out = u''
        start_tag = u''
        end_tag = u''

        if prettyPrintEnabled:
            start_tag = Fore.MAGENTA
            end_tag = Style.RESET_ALL
        for line_number in range(0,len(self.sentences)):
            if line_number in self.best_index:
                out += start_tag + self.sentences[line_number] + end_tag
            else:
                out += self.sentences[line_number]
        if prettyPrintEnabled:
            print("\nINFO: Summary (highlighted in original passage) is as below:\n")
            print out
        print("\nINFO: Actual summary is as below:\n")
        print(self.summary)
        
    def printWordDict(self):
        for word, word_details_list in self.word_dict.items():
            for word_detail in word_details_list:
                word_detail.dump()
                
    def getWordListOrderedBySentence(self, word_list):
        word_details_list = []
        
        for word in word_list:
            word_detail_list = self.word_dict.get(word)
            if (word_detail_list != None):
                for word_detail in word_detail_list:
                    word_details_list.append(word_detail)
            
        return sorted(word_details_list, key=self.bySentenceNumber)
    
    def getWordListOrderedByFrequency(self, word_list):
        word_details_list = []
        
        for word in word_list:
            word_detail_list = self.word_dict.get(word)
            if (word_detail_list != None):
                for word_detail in word_detail_list:
                    word_detail.count = len(word_detail_list)
                    word_details_list.append(word_detail)

      
        return sorted(word_details_list, key=self.byWordFrequency, reverse = True)


    # Improves the proposed summary.
    # We ned to implement this function.    
    def scoreAndGetSummary(self, similarity_threshold = 0.9):
        # We currently have support for detecting and dropping similar
        # sentences. This can be signaled via the DROP_SIMILAR_SENTENCES enum.
        
        # TODO(ananta/thens)
        # Implement other strategies for better summaries.
        # One option may be to look at keywords in the header and footer
        # of the paragraph and give weights to chains which have the words
        # in those sections.
        # Other options could be as below:
        # 1. Keep words in a chain which compare well with their counterparts 
        #    in the chain
        # 2. Once we pick a word to be in a chain, we should remove that word
        #    from other chains?
        
        if self.summarizer_sentence_control == SummarizerSentenceControl.DEFAULT:
            print(self.proposed_index)
            self.best_index = self.proposed_index
            for index in self.best_index:
                self.best_summary.append(self.sentences[index])
            return
        
        print("INFO: Detecting similar sentencs")

        # Run pairwise cosine similarities on the proposed sentences.
        # We keep only one sentence in the list of sentences which have
        # similarities of 0.9 or more.
        dictListSentencesSimilarities = {}
        
        #for idx1 in range(0, len(self.proposed_index)):
        for idx1 in self.proposed_index:
            listIndexes = []
            dictListSentencesSimilarities[idx1] = listIndexes
            for idx2 in self.proposed_index:
            #for idx2 in range(0, len(self.proposed_index)):
                if (idx2 <= idx1):
                    continue
                
                similarity = self.sentence_similarity(self.sentences[idx1], self.sentences[idx2])
                listIndexes = []
                if (similarity >= similarity_threshold):
                    listIndexes.append(idx2)
            dictListSentencesSimilarities[idx1] = listIndexes

        # Iterate through the sentence similarities map and keep only
        # unique sentences. Doing a string match to see if the placeholder
        # output list contains the string is not the best approach. Needs
        # some work.
        output_summary = []
        for key, values in dictListSentencesSimilarities.items():
            if self.sentences[key] not in output_summary:
                output_summary.append(self.sentences[key])
                self.best_index.append(key)
        
        for index in self.best_index:
            self.best_summary.append(self.sentences[index])
        self.best_index.sort()
        print("INFO: Dropped %d sentences" %(len(self.proposed_index) - len(self.best_index)))
        print("INFO: Done detecting similar sentences")
            
    # This function has been copied from http://nlpforhackers.io/textrank-text-summarization/            
    def sentence_similarity(self, sent1, sent2):
        sent1 = [w.lower() for w in sent1]
        sent2 = [w.lower() for w in sent2]
     
        all_words = list(set(sent1 + sent2))
     
        vector1 = [0] * len(all_words)
        vector2 = [0] * len(all_words)
     
        # build the vector for the first sentence
        for w in sent1:
            vector1[all_words.index(w)] += 1
     
        # build the vector for the second sentence
        for w in sent2:
            vector2[all_words.index(w)] += 1
     
        return 1 - cosine_distance(vector1, vector2)

    # Used for sorting the word details list by sentence number.
    def bySentenceNumber(self, word_detail):
        return word_detail.sentence_number
    
    def byWordFrequency(self, word_detail):
        return word_detail.count
    
