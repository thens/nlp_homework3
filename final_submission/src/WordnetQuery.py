# -*- coding: utf-8 -*-
"""
===============================================================================
              Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""
from nltk.corpus import wordnet as wn

class WordnetQuery:
    INCLUDE_LEVEL_ONE_RELATIONS = False
    # http://compprag.christopherpotts.net/wordnet.html#overview
    def synset_method_values(self, synset):
        """
        For a given synset, get all the (method_name, value) pairs
        for that synset. Returns the list of such pairs.
        """
        name_value_pairs = []
        # All the available synset methods:
        method_names = ['hypernyms', 'instance_hypernyms', 'hyponyms', 'instance_hyponyms', 
                        'member_holonyms', 'substance_holonyms', 'part_holonyms', 
                        'member_meronyms', 'substance_meronyms', 'part_meronyms' ]
        for method_name in method_names:
            # Get the method's value for this synset based on its string name.
            method = getattr(synset, method_name)
            vals = method()
            name_value_pairs.append([method_name, vals, 2])
        # additional methods
        #   synset object is a synonym
        name_value_pairs.append(["synonyms", [synset], 3])
        if self.INCLUDE_LEVEL_ONE_RELATIONS:
            #   one level more hypernyms
            name_value_pairs.append(["grandparent_hypernyms", self.get_grandparent_hypernyms(synset), 1])
            #  sibling hyponyms
            name_value_pairs.append(["sibling_hyponyms", self.get_sibling_hyponyms(synset), 1])
        return name_value_pairs
    def get_grandparent_hypernyms(self, synset):
        parent_hypernyms = synset.hypernyms()
        vals = []
        for ph in parent_hypernyms:
            for gph in ph.hypernyms():
                vals.append(gph)
        return vals
    def get_sibling_hyponyms(self, synset):
        parent_hypernyms = synset.hypernyms()
        vals = []
        for ph in parent_hypernyms:
            for sh in ph.hyponyms():
                if (sh != synset):
                    vals.append(sh)
        return vals
    def lemma_method_values(self, lemma):
        """
        For a given lemma, get all the (method_name, value) pairs
        for that lemma. Returns the list of such pairs.
        """
        name_value_pairs = []
        # All the available synset methods:
        method_names = [ 'antonyms' ]
        for method_name in method_names:
            # Check to make sure the method is defined:
            if hasattr(lemma, method_name):
                method = getattr(lemma, method_name)
                # Get the values from running that method:
                vals = method()
                name_value_pairs.append([method_name, vals, 1])
        return name_value_pairs

    def wordnet_relations(self, word1, word2):
        """
        Uses the lemmas and synsets associated with word1 and word2 to
        gather all relationships between these two words. There is
        imprecision in this, since we range over all the lemmas and
        synsets consistent with each (string, pos) pair, but it seems
        to work well in practice.
        
        Arguments:
            word1, word2 (str, str) -- (string, pos) pairs
            
            Value:
                rels (set of str) -- the set of all WordNet relations that hold between word1 and word2
        """
        # Output set of strings:
        rels = []
        # Loop through both synset and lemma relations:
        for lem1 in wn.lemmas(word1, 'n'):
            lemma_methodname_value_pairs = self.lemma_method_values(lem1)
            synset_methodname_value_pairs = self.synset_method_values(lem1.synset())
            for lem2 in wn.lemmas(word2, 'n'):
                # Lemma relations:
                for rel, rel_lemmas, weight in lemma_methodname_value_pairs:
                    if lem2 in rel_lemmas:
                        rels.append([rel, weight])
                # Synset relations:
                for rel, rel_synsets, weight in synset_methodname_value_pairs:
                    if lem2.synset() in rel_synsets:
                        rels.append([rel, weight])
        is_related = False
        if (len(rels) > 0):
            is_related = True
        return is_related, rels

#w = WordnetQuery()
#print (w.wordnet_relations('tree', 'forest'))
#print (w.getHypernyms("rat"))
#print (w.getHypernyms("person"))
