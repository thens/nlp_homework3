# -*- coding: utf-8 -*-
"""
===============================================================================
              Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""

from WordnetQuery import WordnetQuery
from collections import Counter
from nltk.corpus import stopwords
import networkx as nx
import math

# https://stackoverflow.com/questions/196345/how-to-check-if-a-string-in-python-is-in-ascii
def is_ascii(s):
    return all(ord(c) < 128 for c in s)

class LexicalGraph:
    def __init__(self, words, include_level_one_relation=True):
        self.G = nx.Graph()
        self.wn = WordnetQuery()
        self.wn.INCLUDE_LEVEL_ONE_RELATIONS = include_level_one_relation
        # add all the words to the graph
        self.add_words(words)
    def filter_words(self,words):
        # filter only alpha-numeric 
        oc = len(words)
        words = [w for w in words if w.isalnum()]
        nonalphanum = len(words)
        words = [w for w in words if not w in stopwords.words('english')]
        nonstop = len(words)
        print "INFO: Filtered ", oc - nonalphanum, " words that are not alphanumeric"
        print "INFO: Filtered ", nonalphanum - nonstop, " words that are stop words"
        return words
        
    def add_words(self,words_raw):
        words = self.filter_words(words_raw)

        wc = Counter(words)
        
        self.G.add_nodes_from(wc.keys())
        for key,value in wc.items():
            self.G.nodes[key]['count'] = value
            self.G.nodes[key]['word'] = key
            self.G.nodes[key]['pos'] = words.index(key)

    def getNextNodeRelation(self,sorted_nodes,src, idx):
        if idx <len(sorted_nodes)-1:
            dest = sorted_nodes[idx+1]
            try:
                relation = self.G[src][dest]['relation']
            except KeyError:
                #print "Starting next level lookup for", src, " -> ", dest, " index :", idx+1
                for cn in sorted_nodes:
                    try:
                        relation = self.G[src][cn]['relation']
                    except:
                        continue
                    return cn
            return dest
        else:
            return None

    def printChains(self):
        chains = self.getChains()
        for chain_id in sorted(chains, key=lambda k: chains[k]['score_by_homogenity'], reverse=True):
            print chains[chain_id]['chain_summary']
        
    # Returns the list of chains from the graph. The chains are returned
    # in a dictionary which is keyed by the chain number.         
    def getChains(self):
        #nx.draw(self.G, with_labels=True, font_weight='bold')
        ccs = nx.connected_components(self.G)
        chain_id = 1
        chains = {}
        for cc in ccs:
            chain_string = []
            chain_details = []
            total_count = 0
            sorted_nodes = sorted(cc,key=lambda x: self.G.nodes[x]['pos'])
            unique_count = len(sorted_nodes)
            if len(sorted_nodes) <= 1:
                continue
            for idx in range(0,len(sorted_nodes)):
                n = sorted_nodes[idx]
                total_count += self.G.nodes[n]['count']
                src = n
                if is_ascii(n) == False:
                    continue
                chain_string.append('{}({})'.format(self.G.nodes[n]['word'], self.G.nodes[n]['count']))
                dest = self.getNextNodeRelation(sorted_nodes,src,idx)
                if dest is not None:
                    chain_details.append('  link  added: ' +
                                         self.G.nodes[src]['word'] +
                                         '  -> ' + 
                                         self.G[src][dest]['relation'] +
                                         '  -> ' + 
                                         self.G.nodes[dest]['word'])
            homogenity_index = 1 - (1.0 * unique_count / total_count)
            score_by_homogentiy = total_count
            if homogenity_index > 0:
                score_by_homogentiy = score_by_homogentiy * homogenity_index
            chain_summary = "Chain {}: ".format(chain_id) + ', '.join(chain_string)
            chain_summary += "\n  scores: length = {}, homogenity_index = {:5.3f}, homogenity_length = {:5.3f}\n".format(total_count, homogenity_index, score_by_homogentiy) 
            if len(chain_details):
                chain_summary += "\n".join(chain_details)
            chains[chain_id] = {
                    'words' : map(lambda n: self.G.nodes[n]['word'], sorted_nodes),
                    'score_by_length' : total_count,
                    'score_by_homogenity' : score_by_homogentiy,
                    'homogenity_index' : homogenity_index,
                    'chain_summary' : chain_summary,
                    }
            chain_id += 1
        return chains

   
    # Returns the list of chains from the graph. The chains are returned
    # in a dictionary which is keyed by the chain number.         
    def getTopKChains(self, homogenity=True):
        chains = self.getChains()

        if homogenity:
            # get the average 
            average = 1.0 * sum(map(lambda id: chains[id]['score_by_homogenity'], chains.keys())) / len(chains.keys())
            variance = 1.0 * sum(map (lambda id: (chains[id]['score_by_homogenity']-average)**2, chains.keys())) / len(chains.keys())
            sd = math.sqrt(variance)
    
            #print "Average is :", average
            #print "Variance is :", variance
            #print "SD is :", sd
            ids = filter(lambda id: chains[id]['score_by_homogenity'] >= average + 1*sd, chains.keys())        
            ids = sorted(ids, key=lambda id:chains[id]['score_by_homogenity'], reverse=True)
        else:
            ids = chains.keys()
            ids = sorted(ids, key=lambda id:chains[id]['score_by_length'], reverse=True)
        return ids
    
    def updateLinks(self):
        # loop over all words and find if related
        for n1 in self.G.nodes():
            for n2 in self.G.nodes():
                if (n1 == n2):
                    continue
                is_related, rels = self.wn.wordnet_relations(n1, n2)
                if is_related is False:
                    continue
                for rel, wt in rels:
                    #print wt, rels
                    self.G.add_edge(n1, n2, weight=wt, relation=rel)
                    
#LG = LexicalGraph(['yellow', 'blue', 'red', 'ball', 'color', 'black', 'white', 'red', 'blue'])
#LG = LexicalGraph(['yellow', 'blue', 'red', 'ball', 'color', 'black', 'white', 'red', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue'])
#LG = LexicalGraph(['girl', 'hat', 'skirt', 'woman', 'women', 'pants', 'clothes', 'person', 'shirt', 'she'])

#LG = LexicalGraph(['yellow', 'blue', 'red', 'ball', 'color', 'black', 'girl', 'black', 'white', 'red', 'black', 'black', 'black', 'black', 'black', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'girl', 'hat', 'skirt', 'woman', 'women', 'pants', 'clothes', 'person', 'shirt', 'she'], include_level_one_relation=False)
#LG.updateLinks()
#LG.printChains()
#print LG.getTopKChains(homogenity=True)
#print LG.getTopKChains(homogenity=False)
