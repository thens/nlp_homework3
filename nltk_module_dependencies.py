
# -*- coding: utf-8 -*-
"""
===============================================================================
              Homework 3 - Lexical Chains
 			CS 585 - Natural Language Processing, Spring 2018
 			Anantanarayanan G Iyengar (aiyengar@hawk.iit.edu)
 			  Thenappan Meyyappan (tmeyyappan@hawk.iit.edu)
 					 Department of Computer Science
 					Illinois Institute of Technology
===============================================================================
"""

# This file installs the nltk modules needed for executing the lexical chain assignment. 

import nltk

# Please add to this list as needed.
dependencies = ['punkt', 'averaged_perceptron_tagger', 'wordnet', 'stopwords']

for dependency in dependencies:
	nltk.download(dependency)
